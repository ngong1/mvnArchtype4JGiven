package ${package};

import static ${package}.Constant.*;import static ${package}.TestConstant.*;

import java.io.File;
import java.io.FilenameFilter;
import java.util.function.Predicate;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.Test;

import com.tngtech.jgiven.junit.*;
import com.tngtech.java.junit.dataprovider.*;

@RunWith(DataProviderRunner.class)
public class ${class}Test extends ScenarioTest<${class}Given,${class}When,${class}Then>{

	private static Logger l = LoggerFactory.getLogger(${class}Test.class);

	@BeforeClass
	public static void setUpClass() throws Exception {
		System.setProperty("jgiven.report.dir", "target/jgiven-reports/json");
		System.setProperty("jgiven.report.text", "false");
	}

	@Test
	// @formatter:off in Eclipse: Windows → Preferences Java → Code Style → Formatter / Edit
	@DataProvider(value = {
		"             |              |       |       | false", 
		}, splitBy = "\\|", trimValues = true)
	// @formatter:on
	public void commandline_test(String arg0, String arg1, String arg2, String arg3, boolean excaption_thrown) {
		
		l.debug("");
		given().somePreparation(makeArguments(arg0, arg1, arg2, arg3));
		when().someProcessing();
		then().someResult(excaption_thrown);

	}

	/**
	 * 
	 * @param arg0
	 *            from DataProvider index 0
	 * @param arg1
	 *            from DataProvider index 1
	 * @param arg2
	 *            from DataProvider index 2
	 * @return String[] args
	 */
	private String[] makeArguments(String arg0, String arg1, String arg2, String arg3) {
		String[] r;
		
		Predicate<String> p = (s)-> ( s != null && !s.isEmpty());
		if (p.test(arg3)) {
			r = new String[] { arg0, arg1, arg2, arg3 };
		} else if (p.test(arg2)) {
			r = new String[] { arg0, arg1, arg2 };
		} else if (p.test(arg1)) {
			r = new String[] { arg0, arg1 };
		} else if (p.test(arg0)) {
			r = new String[] { arg0 };
		} else {
			r = new String[0];
		}

		return r;
	}

	@After
	public void removeSomeResults() {
		File[] outs = (new File(TEST_RESOURCES)).listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File arg0, String arg1) {
				// TODO Auto-generated method stub
				return arg1.contains("_out.");
			}

		});

		if (outs !=null) {
			for (File out : outs) {
				l.debug(String.format("file to be deleted: %s", out.getAbsolutePath()));
				FileUtils.deleteQuietly(out);
			}
		}
	}

}