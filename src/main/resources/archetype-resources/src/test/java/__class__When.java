package ${package};

import static ${package}.Constant.*;
import static org.junit.Assert.*;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.annotation.*;

import ${package}.*;

public class ${class}When extends Stage<${class}When> {

	private static Logger l = LoggerFactory.getLogger(${class}When.class);

	@ExpectedScenarioState
	String args[];
	@ExpectedScenarioState
	boolean exceptionFired;

	@ProvidedScenarioState
	String result = null;

	public ${class}When someProcessing() {

		if (!exceptionFired) {
			try {
				this.exceptionFired = false;
				App.main(args);
			} catch (${class}Exception e) {
				this.exceptionFired = true;
			}
		}

		return self();
	}

}
