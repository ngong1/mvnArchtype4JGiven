package ${package};

import static ${package}.Constant.*;

public interface TestConstant {

	public static final String TEST_RESOURCES = "src"+FS+"test"+FS+"resources"+FS;

}
