package ${package};

import static ${package}.Constant.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.annotation.ProvidedScenarioState;

import ${package}.${class};

public class ${class}Given extends Stage<${class}Given> {
	
	private static Logger l = LoggerFactory.getLogger(${class}Given.class);

	@ProvidedScenarioState
	boolean exceptionFired = false;
	@ProvidedScenarioState
	String[] args;
	
	public ${class}Given somePreparation(String[] args) {
		this.args = args;
		return self();
	}
	
}