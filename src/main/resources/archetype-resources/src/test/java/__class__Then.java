package ${package};

import static org.hamcrest.MatcherAssert.*;
import static ${package}.Constant.*;
import static org.hamcrest.CoreMatchers.*;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.annotation.ExpectedScenarioState;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ${class}Then extends Stage<${class}Then> {

	private static Logger l = LoggerFactory.getLogger(${class}Then.class);

	@ExpectedScenarioState
	boolean exceptionFired;
	@ExpectedScenarioState
	String result = null;

	public ${class}Then someResult(boolean expection_thrown) {

		assertThat(this.exceptionFired, is(expection_thrown));

		return self();
	}
	
}
