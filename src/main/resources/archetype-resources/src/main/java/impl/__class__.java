/* 
 * 
 */
 
package ${package}.impl;

import static ${package}.Constant.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ${class} implements ${package}.${class} {
	private static Logger l = LoggerFactory.getLogger(${class}.class);

	/**
	 * a default constructor for ${class}
	 */
	public ${class}() {
		l.debug("${class} created");
	}

}
