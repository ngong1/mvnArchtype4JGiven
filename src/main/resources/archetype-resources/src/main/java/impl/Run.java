/**
 * 
 */
package ${package}.impl;

import static ${package}.Constant.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Run implements ${package}.Run {
	private static Logger l = LoggerFactory.getLogger(Run.class);

	public Run() {
		l.debug(f("instantiated %s", Run.class.getName()));
	}

	/**
	 * @param args
	 */
	@Override
	public boolean run(final String[] args) {
		boolean result = true;

		try {
			l.info(f("start %s version %s no of argumens: %d", Run.class.getName(), VERSION, args.length));

			// check parameters
			if (args.length == 0) {
				l.debug("no parameters given");
			} else {
			}

		} catch (Exception e) {
			l.error(st(e));
			result = false;
		}
		
		return result;
	}
	
	public boolean run() {
		return this.run(new String[0]);
	}


	@Override
	public void clear() {
	}

}
