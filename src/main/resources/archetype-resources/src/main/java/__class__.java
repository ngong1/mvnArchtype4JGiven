/**
 * 
 */
package ${package};

public interface ${class} {

    //@todo: the interface specification
	
    /**
     * factory
     */
    public class Factory {
    	
    	/**
    	 * a convenient default instantiation of ${class}
    	 */
    	public static ${class} ${obj} = newInstance();
    	
    	/**
    	 * get a new instance of ${class}
    	 */
    	public static ${class} newInstance() {
    		return new ${package}.impl.${class}();
    	}
    }
    
}
