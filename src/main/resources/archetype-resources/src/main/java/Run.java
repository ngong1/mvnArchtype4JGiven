/**
 * 
 */
package ${package};

import java.io.InputStream;
import java.util.List;

/**
 * @author rsc
 *
 */
public interface Run {

	/**
	 * run the application
	 * @param args<BR/>
	 */
	public boolean run(final String[] args) throws ${class}Exception;
	
	public boolean run();
	
	public void clear();
	
	public class Factory {
		public static Run newInstance() {
			return new ${package}.impl.Run();
		}
	}

}
