/**
 * 
 */
package ${package};

import static ${package}.Constant.*;
import ${package}.Run;
import ${package}.${class}Exception;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 */
public class App {
	private static Logger l = LoggerFactory.getLogger(App.class);

	public App() {
	}
	
	/**
	 * @param args<BR/>
	 */
	public static void main(String[] args) throws ${class}Exception {
		l.info(String.format("start %s with %d arguments %s", l.getName(), args.length, Arrays.toString(args)));

		Run run = Run.Factory.newInstance();
		run.clear();
		String result = f("%s version %s finished with%s unexpected failures", l.getName(), VERSION,
				run.run(args) ? "out" : "");

		l.info(result);
		System.out.println(result);
	}
}