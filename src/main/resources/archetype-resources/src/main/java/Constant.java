/**
 * 
 */
package ${package};

import java.util.Arrays;

public class Constant {
	
	public static final String VERSION = "0.0.1-SNAPSHOT";
	public static final String NL = System.getProperty("line.separator");
	public static final String FS = System.getProperty("file.separator");
	public static final String TMP = System.getProperty("java.io.tmpdir") + FS;

	public static String f(String format, Object... args) {
		return String.format(format, args);
	}

	public static String st(Throwable e) {
		if (e == null) {
			return "null exception";
		}
		return f("unexpected exception: %s: %s\n%s", e.getClass(), e.getMessage(),
				((e.getCause() == null) ? Arrays.toString(e.getStackTrace()).replaceAll(", ", NL)
						: Arrays.toString(e.getCause().getStackTrace()).replaceAll(", ", NL)));
	}

}
