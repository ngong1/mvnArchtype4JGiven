package ${package};

/**
 * use a specific exception for this bundle
 * @author rsc
 * in the catch clause  for Exception e:
			StringWriter s = new StringWriter();
			e.printStackTrace(new PrintWriter(s));
			throw new ${class}Exception(s.toString());
 */

@SuppressWarnings("serial")
public class ${class}Exception extends Exception {
	
	public ${class}Exception(String message) {
		super(message);
	}

	public ${class}Exception(String message, Throwable t) {
		super(message, t);
	}

}

