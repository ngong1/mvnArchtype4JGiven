after creating a project from the prjbase archetype
- run Maven generate-sources
	SHIFT-ALT-G (if set as Eclipse shortcut) or
	right-click project / Run As / Maven generate-sources
- use target/generated-sources/jjtree as Source Folder
	right-click that folder / Build Path / Use as Source Folder
	